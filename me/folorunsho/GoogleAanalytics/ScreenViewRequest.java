package me.folorunsho.GoogleAanalytics;


/**
 * 
 *  @author Emmanuel Ibikunle
 * @email ibk.com@gmail.com
 * 
 * Lets not forget what we wanna achieve here..
 * We want a situation
 */
public class ScreenViewRequest extends BaseRequest {
	
	String screenName;
	
	
	public ScreenViewRequest(String screenName){
		super();
		
		if (GATracker.isEmpty(screenName)) {
			throw new IllegalArgumentException("Screen name must not be empty.");
		}
		
		this.screenName = screenName;
	}
	
	public String getScreenName(){
		return screenName;
	}

}
