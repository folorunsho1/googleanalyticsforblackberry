package me.folorunsho.GoogleAanalytics;

import java.util.Random;

import me.folorunsho.GoogleAanalytics.network.HttpRequestDispatcher;
import me.folorunsho.GoogleAanalytics.network.ThreadPool;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.Display;

/**
 * 
 * @author Emmanuel Ibikunle
 * @email ibk.com@gmail.com
 *
 */
public class GATracker {
	
	public static String TRACKING_ID;
	public static String APP_NAME;
	public static String APP_ID;
	public final static String GOOGLE_ANALYTICS_ENDPOINT = "http://www.google-analytics.com/collect";
	
	private final static String PROTOCOL_VERSION = "1";
	private final static String CLIENT_ID = generateRandom(32);
	//private final static String HIT_TYPE_PAGEVIEW = "pageview";
	private final static String HIT_TYPE_SCREENVIEW = "screenview";
	//private final static String HIT_TYPE_EVENT = "event";
	
	static ThreadPool pool = new ThreadPool(1);
	
	/**
	 * The instance available to the app from outside..
	 */
	public static GATracker INSTANCE = null;
	
	private GATracker(String trackingID, String appName){
		TRACKING_ID = trackingID;
		APP_NAME = appName;
		
		/**  I pray this should have been set b4 it is called **/
		APP_ID = generateRandom(25);
	}
	
	public static synchronized GATracker getInstance(final String trackingID, final String appName) {
		if (INSTANCE == null) {
			INSTANCE = new GATracker(trackingID, appName);
		}
		
		return INSTANCE;
	}
	
	public static void addToQueue(BaseRequest request){
		if(request == null){
			throw new IllegalArgumentException("####### GoogleAnalytis: Request cannot not be null.");
		}
		
		URLEncodedPostData postData = prepareCommonPayload();
		
		if(request instanceof ScreenViewRequest){
			
			ScreenViewRequest screenViewRequest = (ScreenViewRequest) request;
			//Hit Type..
		    postData.append("t", HIT_TYPE_SCREENVIEW);
			//Screen Name
		    postData.append("cd", screenViewRequest.getScreenName());
		}
		else{
			throw new IllegalArgumentException("####### GoogleAnalytis: Unknown Request Type.");
		}
		
		
		final HttpRequestDispatcher googleAnalyticsRunnable;
		googleAnalyticsRunnable = new HttpRequestDispatcher(
			GOOGLE_ANALYTICS_ENDPOINT, "POST" 
		);		
		
		googleAnalyticsRunnable.setData(postData);
		
		new Thread(new Runnable(){
			public void run(){
				pool.assign(googleAnalyticsRunnable);
				pool.complete();
			}
		}).start();

	}
	
	private static URLEncodedPostData prepareCommonPayload(){
		URLEncodedPostData postData = null;
	    postData = new URLEncodedPostData("UTF-8", false);
	    
	    // Version
	    postData.append("v", PROTOCOL_VERSION);
	    //Tracking Id
	    postData.append("tid", TRACKING_ID);
	    //Client Id
	    postData.append("cid", CLIENT_ID);    
	    /** End Required For PageView Hot Type **/	    
	    /** Adding Java Info .. */
	    postData.append("je", "1");	    
	    /** Adding User Language **/
	    postData.append("ul", "en-us");	   
	    /** uUser Agent.. */
	    postData.append("ua", HttpRequestDispatcher.getUserAgent());	     
	    /* Application Tracking..*/
	    // Application Name
	    postData.append("an", APP_NAME);
	    // Application ID/PackageName
	    postData.append("aid", "com.varsoftng.savealife");
	    // Application Version
	    postData.append("av", ApplicationDescriptor.currentApplicationDescriptor().getVersion());	    
	    // Screen Resolution..
	    postData.append("sr", getScreenWidth() + "x" + getScreenHeight());    
	    // Avoid caching...
	    postData.append("z", generateRandom(12));
	    
	    return postData;
	}
	
	private static int getScreenWidth(){
		return Display.getWidth();
	}
	
	private static int getScreenHeight(){
		return Display.getHeight();
	}
	
	 public static String generateRandom(int length) {
	    Random random = new Random();
	    char[] digits = new char[length];
	    digits[0] = (char) (random.nextInt(9) + '1');
	    for (int i = 1; i < length; i++) {
	        digits[i] = (char) (random.nextInt(10) + '0');
	    }
	    return new String(digits);
	 }
	 
	public static final boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}
	
}
