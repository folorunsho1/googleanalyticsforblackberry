package me.folorunsho.GoogleAanalytics.network;

/**
 * 
* @author Emmanuel Ibikunle
 * @email ibk.com@gmail.com
 * 
 */


import java.io.IOException;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.io.http.HttpProtocolConstants;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.Branding;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.WLANInfo;

public class HttpRequestDispatcher implements Runnable {
	protected Done done;
	
	public String method = "GET"; // GET or POST
	
	public synchronized String getMethod() {
		return method;
	}

	public synchronized void setMethod(String method) {
		this.method = method;
	}

	private String url;
	private URLEncodedPostData _postParameters;
	
	public HttpRequestDispatcher(String url)
	{
		this.url = url;
	}
	
	public HttpRequestDispatcher( String url, String _method)
	{
		this.url = url;
		this.method = _method;
	}
	
	public void setData( URLEncodedPostData postParameters){
		_postParameters = postParameters;
	}
	
	private void doHttpGet(){
        //int code;
		try {
		    
		    HttpConnection connection = (HttpConnection)Connector.open(url + getConnectionString());
			connection.setRequestMethod(method);
			
			connection.setRequestProperty("x-rim-transcode-content", "none");
			connection.setRequestProperty("User-Agent",getUserAgent());
			
			System.out.println("###### GoogleAnalytics: [GET] Starting connection to: " + url);
				        
			int responseCode = connection.getResponseCode();
			
			if (responseCode != HttpConnection.HTTP_OK) {
				System.out.println("###### GoogleAnalytics: Unexpected response code:" + responseCode);
				connection.close();
				return;
			}
			else{
				System.out.println("###### GoogleAnalytics: Response code:" + responseCode);
				connection.close();
			}
		} catch (IOException ex) {
			System.out.println("###### GoogleAnalytics: UnexpectedException" + ex.getMessage());
		}
	}
	
	private void doHttpPost(){
		HttpConnection hc = null;

			System.out.println("###### GoogleAnalytics: [Post] Starting connection to: " + url);
			
		try{
			hc = (HttpConnection)Connector.open(url+getConnectionString());
		
			URLEncodedPostData oPostData = (URLEncodedPostData)_postParameters; 
			
			//Set Request Type..
			hc.setRequestMethod(HttpConnection.POST);
			hc.setRequestProperty("x-rim-transcode-content", "none");
			hc.setRequestProperty("User-Agent",getUserAgent());
			
			//Content Type..
			hc.setRequestProperty(HttpProtocolConstants.HEADER_CONTENT_TYPE, oPostData.getContentType());
			//Size of request..
			byte [] postBytes = oPostData.getBytes();
			//Set Content Length..
			hc.setRequestProperty(HttpProtocolConstants.HEADER_CONTENT_LENGTH, Integer.toString(postBytes.length));
			
			//Open an output stream..
			OutputStream strmOut = hc.openOutputStream();
			strmOut.write(postBytes);
			strmOut.flush();
			strmOut.close();
		
			
			int responseCode = hc.getResponseCode();
			
			if (responseCode != HttpConnection.HTTP_OK) {
				System.out.println("###### GoogleAnalytics: Unexpected response code:" + responseCode);
				hc.close();
				return;
			}
			else{
				System.out.println("###### GoogleAnalytics: Response code:" + responseCode);
				hc.close();
			}
		
		}
		catch (Exception e){
			System.out.println("###### GoogleAnalytics: Some Exeption:" + e.getMessage());
			e.printStackTrace();
			System.out.println("###### GoogleAnalytics: ########" );
		}
	
	}
	
	public void run() {
		if(method == "GET" ){
			doHttpGet();
		}
		else{
			doHttpPost();
		}
	}
	
	public static String getUserAgent() {
		
		//BlackBerry 9000: BlackBerry9000/4.6.0.65 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/102
		
		String userAgent;
		try{
			
//			userAgent = "Blackberry" + DeviceInfo.getDeviceName() + "/" +
//					DeviceInfo.getDeviceName() + " Profile/" + System.getProperty(
//					    "microedition.profiles" ) + " Configuration/" + System.getProperty(
//					    "microedition.configuration" ) + " VendorID/" +
//					    Branding.getVendorId();
			userAgent = "BlackBerry"+DeviceInfo.getDeviceName()  +    
								"/" + DeviceInfo.getSoftwareVersion().substring(0, 5) +
								" Profile/" + System.getProperty("microedition.profiles") +
								" Configuration/" + System.getProperty( "microedition.configuration") + 
								" VendorID/" + Branding.getVendorId();
			
			System.out.println("####### userAgent - " + userAgent );
		}
		catch(Exception e){
			System.out.println("####### Exception generation userAgent - " + e.getMessage() );
			userAgent = "Blackberry ";
		}
		
		return userAgent;
	}

	
	/**
	 * Determines what connection type to use and returns the necessary string to use it.
	 * @return A string with the connection info
	 */
	public static String getConnectionString()
	{
		// This code is based on the connection code developed by Mike Nelson of AccelGolf.
		// http://blog.accelgolf.com/2009/05/22/blackberry-cross-carrier-and-cross-network-http-connection
		String connectionString = null;                

		// Simulator behavior is controlled by the USE_MDS_IN_SIMULATOR variable.
		if(DeviceInfo.isSimulator())
		{
			//System.out.println("Device is a simulator and USE_MDS_IN_SIMULATOR is true");
			connectionString = ";deviceside=true";
		}                                        

		// Wifi is the preferred transmission method
		else if(WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED)
		{
			//System.out.println("Device is connected via Wifi.");
			connectionString = ";interface=wifi";
		}

		// Is the carrier network the only way to connect?
		else if((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT)
		{
			//System.out.println("Carrier coverage.");

			String carrierUid = getCarrierBIBSUid();
			if(carrierUid == null)
			{
				// Has carrier coverage, but not BIBS.  So use the carrier's TCP network
				//System.out.println("No Uid");
				connectionString = ";deviceside=true";
			}
			else
			{
				// otherwise, use the Uid to construct a valid carrier BIBS request
				//System.out.println("uid is: " + carrierUid);
				connectionString = ";deviceside=false;connectionUID="+carrierUid + ";ConnectionType=mds-public";
			}
		}                

		// Check for an MDS connection instead (BlackBerry Enterprise Server)
		else if((CoverageInfo.getCoverageStatus() & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS)
		{
			//System.out.println("MDS coverage found");
			connectionString = ";deviceside=false";
		}

		// If there is no connection available abort to avoid bugging the user unnecssarily.
		else if(CoverageInfo.getCoverageStatus() == CoverageInfo.COVERAGE_NONE)
		{
			//System.out.println("There is no available connection.");
		}

		// In theory, all bases are covered so this shouldn't be reachable.
		else
		{
			//System.out.println("no other options found, assuming device.");
			connectionString = ";deviceside=true";
		}        

		return connectionString;
	}

	/**
	 * Looks through the phone's service book for a carrier provided BIBS network
	 * @return The uid used to connect to that network.
	 */
	public static String getCarrierBIBSUid()
	{
		ServiceRecord[] records = ServiceBook.getSB().getRecords();
		int currentRecord;

		for(currentRecord = 0; currentRecord < records.length; currentRecord++) {             
			if(records[currentRecord].getCid().toLowerCase().equals("ippp")) {                 
				if(records[currentRecord].getName().toLowerCase().indexOf("bibs") >= 0) {
					return records[currentRecord].getUid();
				}
			}
		}

		return null;
	}
}